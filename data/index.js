const data = [
  {
    date: '09/19/2019',
    imageURL:
      'https://photoworks-wpengine.netdna-ssl.com/wp-content/uploads/2016/06/Stuart-Franklin_-Magnum-Photos.jpg',
    credit: 'Stuart Franklin, Manchester 1986',
    rule:
      '"Ultimately the product that any writer has to sell is not the subject being written about, but who he or she is."'
  },
  {
    date: '09/20/2019',
    imageURL:
      'https://cdn.britannica.com/32/43732-050-97684142/Children-photograph-Seville-Spain-Henri-Cartier-Bresson-1933.jpg',
    credit: 'Henry Cartier-Bresson, 1933',
    rule:
      'Clutter is the labourous phrase that has pushed out the shorter word that means the same thing. Your dentist will ask you if you are experiencing any pain. If he had his own kid in the chair he would say, "Does it hurt?" He would, in short, be himself.'
  },
  {
    date: '09/21/2019',
    imageURL:
      'https://blog.toryburch.com/wp-content/uploads/2017/06/Magnum17_slide1.jpg',
    credit: 'Dennis Stock, California 1968',
    rule:
      "The secret to good writing is to strip every sentence to it's cleanest components. Every word that serves no function, every long word that could be a short word, every adverb that carries the same meaning that's already in a verb weakens the strength of a sentence."
  },
  {
    date: '09/22/2019',
    imageURL:
      'https://static.standard.co.uk/s3fs-public/thumbnails/image/2014/02/19/09/86DOGS1902A.jpg?w968',
    credit: 'Elliott Erwitt, NYC',
    rule:
      '"Writing is hard work. A clear sentence is no accident. Very few sentences come out right the first time, or even the third time. Remember this in moments of despair."'
  },
  {
    date: '09/23/2019',
    imageURL:
      'https://static01.nyt.com/packages/flash/Lens/2009/09/20090915-Showcase-Boogie/018-20090915-Showcase-Boogie.jpg',
    credit: 'Boogie, Bushwick maybe?',
    rule:
      "Writing improves in direct ratio to the number of things we can keep out of it that shouldn't be there. Examine every word you put on paper."
  },
  {
    date: '09/24/2019',
    imageURL:
      'https://69a36c06cbe69e27b9f8-f9b4c4acf743b3601f21d2ea132e0653.ssl.cf1.rackcdn.com/esq001-DUP.jpg',
    credit: 'Christopher Anderson',
    rule: ` I urge people to write in the first person. What I'm always looking for as an editor is a sentence that says something like "I'll never forget the day when I..." I think, "Aha! A person!"`
  },
  {
    date: '09/25/2019',
    imageURL:
      'https://images.squarespace-cdn.com/content/v1/575719aa20c64748d571fdaf/1565994919505-PPBVF0CRVMH56FR5R8PH/ke17ZwdGBToddI8pDm48kBVeX1IyGb6uOqS37goX5-gUqsxRUqqbr1mOJYKfIPR7LoDQ9mXPOjoJoqy81S2I8N_N4V1vUb5AoIIIbLZhVYy7Mythp_T-mtop-vrsUOmeInPi9iDjx9w8K4ZfjXt2dkKhBm3udU5H3-MYV8we-2rhipBTueimTCDri6dvPzYiG6v6ULRah83RgHXAWD5lbQ/NYC14946.jpg?format=500w',
    credit: 'Bruce Gilden',
    rule: `Style is organic to the person doing the writing. The problem of writers who set out deliberately to garnish their prose is that you lose whatever it is that makes you unique. Therefore a fundamental rule is: be yourself.`
  },
  {
    date: '09/26/2019',
    imageURL:
      'https://media.npr.org/assets/img/2015/06/26/mary-ellen-mark-001_custom-6d3ad9b1015b1fb32fec633b836dcfe277ee5464-s800-c85.jpg',
    credit: 'Mary Ellen Mark',
    rule:
      'Sell yourself, and your subject will exert its own appeal. Believe in your own identity and your own opinions. Writing is an act of ego, and you might as well admit it. Use its energy to keep yourself going.'
  },
  {
    date: '09/27/2019',
    imageURL:
      'https://dch81km8r5tow.cloudfront.net/wp-content/uploads/2007/10/Martha-POSTER_LeadShot.jpg',
    credit: 'Martha Cooper',
    rule: `You are writing for yourself. Don't try to visualize the great mass audience. There is no such audience - every reader is different person.`
  },
  {
    date: '09/28/2019',
    imageURL:
      'https://www.maxxi.art/wp-content/uploads/2019/07/Robert_Capa_Sicilian_peasant.jpg',
    credit: 'Robert Capa',
    rule: `"Never say anything in writing that you wouldn't comfortably say in conversation."`
  },
  {
    date: '09/29/2019',
    imageURL:
      'https://static01.nyt.com/images/2017/03/15/blogs/15-lens-elliott-slide-RMXP/15-lens-elliott-slide-RMXP-superJumbo.jpg',
    credit: 'Elliott Erwitt',
    rule: `"The secret of his [Mencken] popularity - aside from his pyrotechnical use of the American language - was he was writing for himself and didn't give a damn what the reader might think."`
  },
  {
    date: '09/30/2019',
    imageURL:
      'https://mediastore.magnumphotos.com/CoreXDoc/MAG/Media/Home2/c/4/4/7/NYC3850.jpg',
    credit: 'Elliott Erwitt',
    rule: `"Who am I writing for?" The question that begins this chapter has irked some readers. They want me to say "Whom am I writing for?" But I can't bring myself to say it. It's just not me.`
  },
  {
    date: '10/01/2019',
    imageURL:
      'http://www.fayenealphotography.co.uk/wp-content/uploads/2019/05/Elliott-Erwitt-Colorado-1955-1030x615-1030x530.jpg',
    credit: 'Elliott Erwitt',
    rule: `"Such matters as rhythm and alliteration are vital to every sentence."`
  }
]

export default data
