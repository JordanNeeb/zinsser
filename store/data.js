import moment from 'moment'
import data from '~/data'

export const state = () => ({
  currentData: data.filter(
    (datum) => datum.date === moment().format('MM/DD/YYYY')
  )[0],
  currentDate: true,
  firstDate: false
})

export const mutations = {
  setViewed(state) {
    const viewed = JSON.parse(window.localStorage.getItem('zinsser')) || []

    if (!viewed.includes(state.currentData.date)) {
      viewed.push(state.currentData.date)
    }

    window.localStorage.setItem('zinsser', JSON.stringify(viewed))
  },
  next(state) {
    if (!state.currentDate) {
      const currentIndex = data.findIndex(
        (datum) => datum.date === state.currentData.date
      )
      const nextData = data[currentIndex + 1]

      state.currentData = nextData
      state.firstDate = false

      if (nextData.date === moment().format('MM/DD/YYYY')) {
        state.currentDate = true
      }
    }
  },
  last(state) {
    if (!state.firstDate) {
      const currentIndex = data.findIndex(
        (datum) => datum.date === state.currentData.date
      )
      const lastData = data[currentIndex - 1]

      state.currentData = lastData
      state.currentDate = false

      if (currentIndex - 1 === 0) {
        state.firstDate = true
      }
    }
  }
}
